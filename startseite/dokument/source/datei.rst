Datei Verwalten
===============

Datei Information
-----------------

.. code-block:: go

   package main

    import (
	  "fmt"
	   "log"
	   "os"
        )

        func main() {

	     FileInfo,err:=os.Stat("start.htm")

	    if err != nil {
		log.Fatal(err)
	     }

	fmt.Println(FileInfo)

     }

Datei ist nicht vorhanden
-------------------------

.. code-block:: go 

   main() {
   if _, err := os.Stat("json/Zugang/zugang.json"); err != nil && os.IsNotExist(err) {

		Datei ist nicht vorhanden

	}
    
    datei vorhanden
    }

Datei speichern
----------------

  .. code-block:: go
 
   package main

   import (
	"io/ioutil"
    )

    func main() {
	Inhalt:= []byte("Willkommen auf meine Seite")
	ioutil.WriteFile("Test", Inhalt, 777)
    }

Extern Datei
------------

.. code-block:: go


    package main

    import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
    )

    func main() {

	Inhalt,err:=http.Get("http://webprogrammieren.de")

	if err != nil {
		log.Fatal(err)
	}

	Ausgabe,_:= ioutil.ReadAll(Inhalt.Body)

	fmt.Println(Ausgabe)
    }


   

