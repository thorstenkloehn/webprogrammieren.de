.. webprogrammieren.de documentation master file, created by
   sphinx-quickstart on Thu Oct 25 13:01:32 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Willkommen auf meine Seite
==========================


.. toctree::
   :maxdepth: 2
   :caption: Inhalt:

   installieren.rst
   grundlagen.rst
   gopherJS.rst
   datei.rst
   server.rst
   Sicherheit.rst
   session.rst
   backup.rst
   impressum.rst
   Datenschutzerklaerung.rst


Bücher
======
Web Programmieren 
-----------------
+  Mastering Go Web Services 
+  Level Up Your Web Apps With Go 
+  Go Web Programming
+  Go: Building Web Applications
+  Go Web Development Cookbook
+  Learning Go Web Development
+  Web Development with Go: Building Scalable Web Apps and RESTful Services

Sicherheit
----------

+ Security with Go



Video 
=====

+ https://www.udemy.com/go-programming-language/
+ https://www.udemy.com/front-end-web-development-using-go/
+ https://www.udemy.com/learning-path-go-advancing-into-web-development-with-go/
+ https://www.udemy.com/go-programlama-dili/
+ https://www.udemy.com/go-the-complete-developers-guide-to-golang-4-in-1/
+ https://www.udemy.com/modern-golang-programming/
+ https://www.udemy.com/go-the-complete-developers-guide/
+ https://www.udemy.com/hands-on-with-go/