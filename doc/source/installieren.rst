Installieren Programme
======================

Go Installieren
---------------
.. code-block:: bash

   wget https://dl.google.com/go/go1.11.linux-amd64.tar.gz
   tar -C /usr/local -xzf go1.11.linux-amd64.tar.gz
   nano /etc/environment
   ------Datei----------
   PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"
   GOPATH="/go"
   ------------------

Git
---
.. code-block:: bash
 
  sudo apt-get install git-core

Datenbank
---------

MySQl
~~~~~
.. code-block:: bash

 sudo apt-get install mysql-server mysql-client
 sudo apt update
 sudo apt install mysql-server
 sudo mysql_secure_installation
 sudo mysql
 ---------Passwort Ändern ----------------------
 mysql > ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
 mysql > exit

Extern Zugriff
^^^^^^^^^^^^^^


.. code-block:: bash

    nano /etc/mysql/mysql.conf.d/mysqld.cnf
     -------Datei ändern
        bind 127.0.0.1  --->bind 0.0.0.0
     sudo /etc/init.d/mysql restart   
    
Extern user zugriff

.. code-block:: bash
    
    mysql -u -root -p
    use mysql;
    update user set host='%' where user='euer_benutzer';
    update db set host='%' where user='euer_benutzer';
    exit;
   

Redis
~~~~~
.. code-block:: bash

 wget http://download.redis.io/releases/redis-5.0.0.tar.gz
 tar xzf redis-5.0.0.tar.gz
 cd redis-5.0.0
 make
 src/redis-server

MongoDB
~~~~~~~

+ https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/



