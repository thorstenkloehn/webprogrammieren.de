package webseiten

import (
	"io/ioutil"
	"net/http"
	"text/template"

	"github.com/gorilla/mux"

	"webprogrammieren.de/struktur"
)

var (
	vorlagen, _ = template.ParseGlob("vorlagen/start/*")
	seiten      struktur.WebSeite
)

func Index(w http.ResponseWriter, r *http.Request) {

	seiten.Titel = "Index"
	inhalt, _ := ioutil.ReadFile("./html/index")
	seiten.Inhalt = string(inhalt)

	vorlagen.ExecuteTemplate(w, "Index.htm", seiten)

}

func Seiten(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	Inhalt := vars["Seiten"]
	seiten.Titel = Inhalt
	inhalt, _ := ioutil.ReadFile("./html/" + Inhalt)
	seiten.Inhalt = string(inhalt)
	vorlagen.ExecuteTemplate(w, "Index.htm", seiten)
}
