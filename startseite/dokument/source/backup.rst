Backup
======
 
.. code-block:: bash
  
  mysqldump -u root -p --all-databases > sicherung.sql 
  zip -r git.zip /git
