Datenbank
=========

.. raw:: html
    
    <iframe width="560" height="315" src="https://www.youtube.com/embed/04-qw993WV4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Extern Zugriff
--------------

.. code-block:: bash

    nano /etc/mysql/mysql.conf.d/mysqld.cnf
     -------Datei ändern
        bind 127.0.0.1  --->bind 0.0.0.0
     sudo /etc/init.d/mysql restart   
    
Extern user zugriff

.. code-block:: bash
    
    mysql -u -root -p
    use mysql;
    update user set host='%' where user='euer_benutzer';
    update db set host='%' where user='euer_benutzer';
    exit;
   