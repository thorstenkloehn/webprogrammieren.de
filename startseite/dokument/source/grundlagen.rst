Grundlagen Go
=============

Installation und Hello World
-----------------------------

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/9GAYj92SqOc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Variablen und Typen
-------------------

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/5tZRDCHkCF8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Funktionen und Arithmetische Operationen
----------------------------------------

.. raw:: html
 
   <iframe width="560" height="315" src="https://www.youtube.com/embed/99ovQ-p4ZLM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Pakete und exports
------------------

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/IwnGDqQg9Ys" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Ablaufsteuerung Bedingungen und Schleifen
-----------------------------------------

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/s0HrzYOhQc4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Das Defer Statement
-------------------
.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/Csj93vs0b_U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Ein simpler Webserver
---------------------

.. raw:: html
   
   <iframe width="560" height="315" src="https://www.youtube.com/embed/pVQD62_H8iM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Zeiger
------

.. raw:: html

  <iframe width="560" height="315" src="https://www.youtube.com/embed/_9yoxd_cvTc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Arrays und Slices
-----------------

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/L9GsBXk9O0c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
   
Variable Anzahl an Argumenten
-----------------------------

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/52ZBCyeiTW8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Strukturen
----------

 .. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/jHsorRbSSKo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Map / Hashmap
-------------
 
.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/kK6-cELeHUA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Methoden
--------

.. raw:: html

     <iframe width="560" height="315" src="https://www.youtube.com/embed/fCw_8nw71y8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Interfaces
----------

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/i0F1nu_m2QY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

switches und casting
--------------------

.. raw:: html

     <iframe width="560" height="315" src="https://www.youtube.com/embed/L5_21A_fSRI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Vordefinierte Interfaces
------------------------

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/tcX3R_C5DB4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Funktionale Programmierung und Closures
---------------------------------------

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/6npK6wZwduU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Concurrency mit Channels Select und Mutexen
-------------------------------------------

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/fbX25cwvDyo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>







