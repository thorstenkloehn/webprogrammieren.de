package main

import (
	"fmt"

	"github.com/gopherjs/gopherjs/js"
)

func start() {

	Testeingabe := js.Global.Get("document").Call("getElementById", "Testeingabe")
	Inhalt := Testeingabe.Get("value").String()
	if Inhalt == "Thorsten" {
		fmt.Println("Richtig eingeben")
		return
	}
	fmt.Println("Falsch eingeben")

}

func main() {

	button := js.Global.Get("document").Call("getElementsByTagName", "testseite")
	test := button.Get("length")
	fmt.Println(test, button)

}
