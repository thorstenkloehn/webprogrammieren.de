package main

import (
	"fmt"
)

func main() {

	start := map[string]string{
		"Steffi": "Meine Freundin",
		"Hallo":  "Hello",
	}

	fmt.Println(start["Hallo"])
}
