Session
=======


Session Installieren
---------------------

.. code-block:: bash
  
    go get github.com/gorilla/sessions

Session Import
--------------

.. code-block:: go

    import "github.com/gorilla/sessions"

Session Beispiel
----------------

Variable Sitzen
~~~~~~~~~~~~~~~

.. code-block:: go

   var (
	key = []byte("Testseite")
	store = sessions.NewCookieStore(key)
        )


Starten
~~~~~~~

.. code-block:: go

    func Start(w http.ResponseWriter, r *http.Request) bool {
	

	session, _ := store.Get(r, "thorsten")

	// Check if user is authenticated
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		
		return false
	}
        return true
        }

Sitzen
~~~~~~

.. code-block:: go

    func Start(w http.ResponseWriter, r *http.Request) {
    session, _ := store.Get(r, "thorsten")

	session.Values["authenticated"] = true
	session.Save(r, w)
	return

    }

Ausloggen
~~~~~~~~~


.. code-block:: go

   func logout(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "cookie-name")

	// Revoke users authentication
	session.Values["authenticated"] = false
	session.Save(r, w)
 }


   