package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {

	Inhalt,err:=http.Get("http://webprogrammieren.de")

	if err != nil {
		log.Fatal(err)
	}

	Ausgabe,_:= ioutil.ReadAll(Inhalt.Body)

	fmt.Println(Ausgabe)
}