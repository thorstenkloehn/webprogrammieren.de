GopherJS
========

Was ist Dom
---------------

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/djRV-fgHKAE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Elemente selektieren 
~~~~~~~~~~~~~~~~~~~~

ID 
^^

index.html
'''''''''''
.. code-block:: html

   <p id="test>hallo</p>

main.go
'''''''

.. code-block:: go

   start:=js.Global.Get("document").Call("getElementById", "test") 


Klasse
^^^^^^
index.html
''''''''''

.. code-block:: html

    <p class="test">Thorsten Klöhn<p>

   


Elementnamen
^^^^^^^^^^^^

Namen 
^^^^^

Selektor 
^^^^^^^^

Was ist  Ajax
-------------

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/_kpfWf4tFfc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Linkliste
---------
+ https://github.com/gopherjs/gopherjs
+ https://siongui.github.io/tag/gopherjs.html
+ https://qiita.com/hajimehoshi/items/bf16816e058f312386f0
+ https://siongui.github.io/2016/01/29/go-gopherjs-synonyms-with-javascript/
+ https://siongui.github.io/2017/12/07/synonyms-go-and-javascript/
Bücher
------

+ Isomorphic Go

Udemy
-----

+ LEARNING PATH: Go: Advancing into Web Development with Go
+ Front-End Web Development using Go
+ Go: The Complete Developer's Guide to Golang: 4-in-1


