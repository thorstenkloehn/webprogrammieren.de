package pandoc

import (
	"html"
	"os/exec"
	"strings"
)

func Ausgaben(format, Text string) string {

	start := exec.Command("pandoc", "-f", "html", "-t", format)
	start.Stdin = strings.NewReader(Text)
	test, _ := start.CombinedOutput()
	start.Run()
	return html.EscapeString(string(test))
}
func Eingabe(format, Text string) string {

	start := exec.Command("pandoc", "-f", format, "-t", "html")
	start.Stdin = strings.NewReader(Text)
	test, _ := start.CombinedOutput()
	start.Run()

	return string(test)
}