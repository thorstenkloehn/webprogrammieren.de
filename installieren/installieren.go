package installieren

import (
	"os"
	"webprogrammieren.de/struktur"
	"encoding/json"
	"io/ioutil"
	"fmt"
	"net/http"
	"github.com/google/uuid"
)

var (
	router = http.NewServeMux()
	server = &http.Server{
		Addr:    ":"+os.Args[1],
		Handler: router, }
		start = uuid.New()
	test  = start.String()
	)
	func Start() {
		fmt.Println(test)
		router.HandleFunc("/", Startseite)
	
		server.ListenAndServe()
	
	}

	func Startseite(w http.ResponseWriter, r *http.Request) {

		if r.FormValue("Senden") == "" {
			http.ServeFile(w, r, "installieren/index.htm")
	
		}
	
		if r.FormValue("UUID") == test {
			zugang := struktur.Zugang{}
		//	zugang.UUID = test
			zugang.Nutzername = r.FormValue("Nutzername")
			zugang.Passwort = r.FormValue("Passwort")
			Inhalt, _ := json.Marshal(zugang)
			ioutil.WriteFile("json/Zugang/zugang.json", Inhalt, 777)
			server.Close()
			return
		}
		
		return
	
	}