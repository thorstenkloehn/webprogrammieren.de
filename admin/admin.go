// Willkommen auf meine Seite


//Weitere Information finden Sie hier unten
//Weitere Information findem Sie hier
package admin

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"text/template"

	"github.com/gorilla/mux"

	"github.com/google/uuid"
	"webprogrammieren.de/sitzung"
	"webprogrammieren.de/struktur"
)

var (

	vorlagen, _ = template.ParseGlob("vorlagen/admin/*")  
	seiten      struktur.WebSeite
	Anmeldung   = struktur.Zugang{}
	Inhalt, _   = ioutil.ReadFile("json/Zugang/zugang.json")
	err         = json.Unmarshal(Inhalt, &Anmeldung)
)

func Login(w http.ResponseWriter, r *http.Request) {

	if r.FormValue("Senden") == "" {
		vorlagen.ExecuteTemplate(w, "login.htm", seiten)
		return
	}

	start := uuid.New()
	GeheimPasswort := start.String()
	ioutil.WriteFile("json/Zugang/GeheimPasswort", []byte(GeheimPasswort), 0777)
	if r.FormValue("Nutzername") == Anmeldung.Nutzername && r.FormValue("Passwort") == Anmeldung.Passwort {
		sitzung.Sitzen(w, r)
		http.Redirect(w, r, "/Admin/", 301)
		return
	}
	http.Redirect(w, r, "/login/", 301)
	return
}

func Index(w http.ResponseWriter, r *http.Request) {

	test := sitzung.Start(w, r)
	if test == true {
		seiten.Titel = "index"
		inhalt, _ := ioutil.ReadFile("./html/index")
		seiten.Inhalt = string(inhalt)
		vorlagen.ExecuteTemplate(w, "index.htm", seiten)
		return
	}

	w.Write([]byte("Sie haben kein Zugriff"))
}

func Ausloggen(w http.ResponseWriter, r *http.Request) {

	test := sitzung.Start(w, r)
	if test == true {
		sitzung.Ausloggen(w, r)
		http.Redirect(w, r, "/", 301)
		return
	}

	http.Redirect(w, r, "/admin/", 301)
	return
}

func AdminSeiten(w http.ResponseWriter, r *http.Request) {
	test := sitzung.Start(w, r)
	if test == true {
		vars := mux.Vars(r)
		Inhaltsangabe := vars["Seiten"]
		seiten.Titel = Inhaltsangabe
		inhalt, _ := ioutil.ReadFile("./html/" + Inhaltsangabe)
		seiten.Inhalt = string(inhalt)
		vorlagen.ExecuteTemplate(w, "index.htm", seiten)
		return
	}

	w.Write([]byte("Sie haben kein Zugriff"))

}

func AdminBearbeitenSeiten(w http.ResponseWriter, r *http.Request) {
	test := sitzung.Start(w, r)
	vars := mux.Vars(r)
	Inhalt := vars["Seiten"]
	if test == true {

		inhalt, _ := ioutil.ReadFile("./html/" + Inhalt)
		seiten.Inhalt = string(inhalt)
		vorlagen.ExecuteTemplate(w, "Seitenbearbeiten.htm", seiten)
		return

	}
	w.Write([]byte("Sie haben kein Zugriff"))
}

func AdminBearbeitenSeitenbearbeiten(w http.ResponseWriter, r *http.Request) {

	test := sitzung.Start(w, r)
	vars := mux.Vars(r)
	if test == true {
		Titel := vars["Seiten"]
		Inhalt := r.FormValue("Inhalt")
		ioutil.WriteFile("./html/"+Titel, []byte(Inhalt), 0777)
		http.Redirect(w, r, "/Admin/"+Titel, 301)
		return

	}
	w.Write([]byte("Sie haben kein Zugriff"))
	return
}

func Vorschau(w http.ResponseWriter, r *http.Request) {
	test := sitzung.Start(w, r)
	vars := mux.Vars(r)
	Inhalt := vars["Seiten"]
	if test == true {

		inhalt, _ := ioutil.ReadFile("./html/" + Inhalt)
		seiten.Inhalt = string(inhalt)
		vorlagen.ExecuteTemplate(w, "Vorschau.htm", seiten)
		return

	}
	w.Write([]byte("Sie haben kein Zugriff"))
}