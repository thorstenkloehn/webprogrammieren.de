package main

import (
	"net/http"
)

func main() {
	mux := http.NewServeMux()
	fs := http.FileServer(http.Dir("dokument/build/html"))
	mux.Handle("/", http.StripPrefix("/", fs))

	http.ListenAndServe(":80", mux)
}
