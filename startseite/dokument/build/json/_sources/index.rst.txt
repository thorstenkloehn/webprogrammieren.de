.. webprogrammieren.de documentation master file, created by
   sphinx-quickstart on Thu Oct 25 13:01:32 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Willkommen auf meine Seite
==========================

.. toctree::
   :maxdepth: 2
   :caption: Inhalt:
   
   installieren.rst
   server.rst
   datenbank.rst
   impressum.rst
   backup.rst
   
