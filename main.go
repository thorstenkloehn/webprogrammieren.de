package main

import (
	"io/ioutil"
	"os"

	"github.com/google/uuid"
	"webprogrammieren.de/installieren"
	"webprogrammieren.de/router"
)

func main() {
	if _, err := os.Stat("json/Zugang/zugang.json"); err != nil && os.IsNotExist(err) {

		installieren.Start()

	}

	start := uuid.New()
	GeheimPasswort := start.String()
	ioutil.WriteFile("json/Zugang/GeheimPasswort", []byte(GeheimPasswort), 0777)

	router.Start()

}
