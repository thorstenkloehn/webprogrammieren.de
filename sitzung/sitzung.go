package sitzung

import (
	"io/ioutil"
	"net/http"

	"github.com/gorilla/sessions"
)

var (
	key, _ = ioutil.ReadFile("json/Zugang/GeheimPasswort")
	store  = sessions.NewCookieStore(key)
)

func Start(w http.ResponseWriter, r *http.Request) bool {

	session, _ := store.Get(r, "thorsten")

	// Check if user is authenticated
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {

		return false
	}
	return true

}

func Sitzen(w http.ResponseWriter, r *http.Request) {

	session, _ := store.Get(r, "thorsten")

	session.Values["authenticated"] = true
	session.Save(r, w)
	return

}

func Ausloggen(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "thorsten")
	session.Values["authenticated"] = false
	session.Save(r, w)
}
