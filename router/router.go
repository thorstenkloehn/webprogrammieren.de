package router

import (
	"net/http"
	"os"

	"webprogrammieren.de/admin"

	"webprogrammieren.de/webseiten"

	"github.com/gorilla/mux"
)

var (
	

	router = mux.NewRouter()
	server = &http.Server{
		Addr:    ":" + os.Args[1],
		Handler: router,
	}
	server1 = &http.Server{
		Addr:    ":443",
		Handler: router,
	}
)

func Start() {

	s := http.StripPrefix("/static/", http.FileServer(http.Dir("static")))
	router.PathPrefix("/static/").Handler(s)
	t := http.StripPrefix("/doc/", http.FileServer(http.Dir("doc/build/html")))
	router.PathPrefix("/doc/").Handler(t)
	router.HandleFunc("/", webseiten.Index)
	router.HandleFunc("/{Seiten}", webseiten.Seiten)
	router.HandleFunc("/login/", admin.Login)
	router.HandleFunc("/Admin/", admin.Index)
	router.HandleFunc("/Admin/Ausloggen/", admin.Ausloggen)
	router.HandleFunc("/Admin/{Seiten}", admin.AdminSeiten)
	router.HandleFunc("/Admin/bearbeiten/{Seiten}", admin.AdminBearbeitenSeiten).Methods("GET")
	router.HandleFunc("/Admin/bearbeiten/{Seiten}", admin.AdminBearbeitenSeitenbearbeiten).Methods("POST")
	router.HandleFunc("/Admin/Vorschau/{Seiten}", admin.Vorschau).Methods("GET")
	router.HandleFunc("/Admin/Vorschau/{Seiten}",admin.AdminBearbeitenSeitenbearbeiten).Methods("POST")
	if os.Args[1] == "443" {
		server1.ListenAndServeTLS("/etc/letsencrypt/live/webprogrammieren.de/cert.pem", "/etc/letsencrypt/live/webprogrammieren.de/privkey.pem")
	} else {
		server.ListenAndServe()
	}

}
