http Server
===========
http/net Beispiel
~~~~~~~~~~~~~~~~~
.. code-block:: go
   
   var (
	router = http.NewServeMux()
	server = &http.Server{
		Addr:    ":8080",
		Handler: router, }

	)

    func main() {
	
	
		server.ListenAndServe()
	
	}


Udemy
~~~~~

+ https://www.udemy.com/go-programming-language/
+ https://www.udemy.com/learning-path-go-advancing-into-web-development-with-go/
+ https://www.udemy.com/go-the-complete-developers-guide-to-golang-4-in-1/
+ https://www.udemy.com/learning-path-go-real-world-go-solutions-for-gophers/

